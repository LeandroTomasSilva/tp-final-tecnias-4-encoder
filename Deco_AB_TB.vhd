--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:27:49 05/20/2018
-- Design Name:   
-- Module Name:   D:/temp/borrar/deco_AB/Deco_AB_TB.vhd
-- Project Name:  decoAB
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DecoAB
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Deco_AB_TB IS
END Deco_AB_TB;
 
ARCHITECTURE behavior OF Deco_AB_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DecoAB
    PORT(
         piAB : IN  std_logic_vector(1 downto 0);
         clk : IN  std_logic;
         reset : IN  std_logic;
         poPulsos_Up : OUT  std_logic;
         poPulsos_down : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal piAB : std_logic_vector(1 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal poPulsos_Up : std_logic;
   signal poPulsos_down : std_logic;

   -- Clock period definitions
   constant clk_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DecoAB PORT MAP (
          piAB => piAB,
          clk => clk,
          reset => reset,
          poPulsos_Up => poPulsos_Up,
          poPulsos_down => poPulsos_down
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;
		
		reset<='1';
      wait for clk_period*10;
		reset<='0';

     wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
     wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="01";
     wait for clk_period*20;
	  piAB<="00";
     wait for clk_period*30;
	  piAB<="01";
     wait for clk_period*10;
	  piAB<="00";
    wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
     wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="01";
     wait for clk_period*20;
	  piAB<="00";
     wait for clk_period*30;
	  piAB<="01";
     wait for clk_period*10;
	  piAB<="00";
		

      -- insert stimulus here 

      wait;
   end process;

END;
