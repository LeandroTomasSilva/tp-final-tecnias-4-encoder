----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:29:39 06/28/2024 
-- Design Name: 
-- Module Name:    velocidadRPM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity velocidadRPM is 
	port(clk: in std_logic; --15khz
	piAB: in std_logic_vector(1 downto 0));
end velocidadRPM;

architecture Behavioral of velocidadRPM is
	signal contador: integer := 0;
	signal max_contador: integer := 0;
	signal velocidad: integer := 0;
	begin

		CONTADOR_PROCESS: process(clk, piAB)
		begin
		
			if clk'event and clk ='0' then
				contador = contador + 1;
			end if;
			
			
			
			if piAB(0)'event and piAB(0)='0' then --Se va a utilizar la se�al A para saber el periodo
				
				max_contador <= contador;
				velocidad = 66*10^(-6) * 500 * max_contador;  --66us es el periodo de nuestro clk, 500 son los pulsos por vuelta, y max contador son los pulsos de clk que se contaron por periodo de pulso A.
				
				contador = 0;
				
			end if;
			
		end process;
		
end Behavioral;

